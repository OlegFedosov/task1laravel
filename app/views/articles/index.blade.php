<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Articles</title>
</head>
<body>
	<div>
		<h1>Articles</h1>
		@foreach($links as $link)
			<div>{{ HTML::link('/article/'  . $link , 'Article' . $link )}}</div>
		@endforeach
	</div>
</body>
</html>
