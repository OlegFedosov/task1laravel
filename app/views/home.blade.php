<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home page</title>
</head>
<body>
	<div>
		<h1>Home Page</h1>
		<div>{{ HTML::link('/articles', 'Articles')}}</div>
		<div>{{ HTML::link('/about', 'About')}}</div>
		<div>{{ HTML::link('/contact', 'Contact Us')}}</div>
	</div>
</body>
</html>
