<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Form</title>
</head>
<body>
	<div>
		<h1>Form</h1>
		<form method="POST" >
			<input type="text" name="name" placeholder="your name" val="{{ $name or 'Default' }}" />
			<input type="text" name="message" placeholder="your message" val="{{ $message or 'Default' }}" />
			<input type="submit" name="submit" value="SEND"/>
		</form>
	</div>
</body>
</html>
