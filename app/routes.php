<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/','HomeController@redirectToHome');
Route::get('/home','HomeController@showHome');
Route::get('/about','AboutController@showAbout');
Route::get('/articles','ArticlesController@showArticles');
Route::get('/article/{id}','ArticlesController@showOneArticle');
Route::post('/contact','ContactController@postContact');
Route::get('/contact','ContactController@showContact');