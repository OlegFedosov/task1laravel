<?php

class ArticlesController extends BaseController {

    public function showArticles()
    {
        return View::make('articles.index', ['links' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]]);
    }

	public function showOneArticle($id)
	{
		return View::make('articles.inner', ['article' => 'Article' . $id]);
	}

}
